import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Akshat on 02-09-2016.
 */
class Cluster {
    ArrayList<Point> points;
    Point mean;

    public Cluster(ArrayList<Point> points) {
        this.points = points;
        this.mean = mean;
    }

    public Cluster() {
        points = new ArrayList<>();
    }

    public ArrayList<Point> getPoints() {
        return points;
    }

    @Override
    public String toString() {
        return Arrays.toString(points.toArray());
    }
}
