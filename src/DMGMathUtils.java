import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Akshat on 02-09-2016.
 */
public class DMGMathUtils {

    static double sumOfSquaredError(ArrayList<Cluster> clusters) {
        double sse = 0;
        for (Cluster cluster : clusters) {
            Point mean = getMean(cluster.getPoints());
            for (Point point : cluster.getPoints()) {
                double error = getDistance(point, mean);
                error = Math.pow(error, 2.0);
                sse += error;
            }
        }
        return sse;
    }

    static double getDistance(Point a, Point b) {
        double distance = 0;
        for (int i = 0; i != Settings.numberOfAttributes; i++) {
            distance += Math.pow(a.attributes[i] - b.attributes[i], 2.0);
        }
        distance = Math.sqrt(distance);
        return distance;
    }

    static ArrayList<Cluster> getClusters(ArrayList<Point> dataPoints, ArrayList<Point> means) {
        Cluster[] clusters = new Cluster[Settings.k];

        for (int i = 0; i < Settings.k; i++) {
            clusters[i] = new Cluster();
        }

        for (Point dataPoint : dataPoints) {
            clusters[getClosestMean(means, dataPoint)].getPoints().add(dataPoint);
        }
        ArrayList<Cluster> clustersArray = new ArrayList<>();
        Collections.addAll(clustersArray, clusters);
        return clustersArray;
    }

    static Point getMean(ArrayList<Point> points) {
        ArrayList<Cluster> a = new ArrayList<>();
        a.add(new Cluster(points));
        return DMGMathUtils.getMeans(a).get(0);
    }

    static ArrayList<Point> getMeans(ArrayList<Cluster> clusters) {             // TODO: 07-09-2016
        ArrayList<Point> means = new ArrayList<>();
        for (Cluster cluster : clusters) {
            Point mean = new Point();
            for (Point point : cluster.getPoints()) {
                for (int i = 0; i != point.attributes.length; i++) {
                    mean.attributes[i] += point.attributes[i];
                }
            }
            for (int i = 0; i != mean.attributes.length; i++) {
                mean.attributes[i] /= cluster.getPoints().size();
            }
            means.add(mean);
        }
        return means;
    }

    static int getClosestMean(ArrayList<Point> means, Point point) {
        double minDistance = Double.MAX_VALUE;
        int minIndex = 0;
        for (int i = 0; i != means.size(); i++) {
            double dist = getDistance(means.get(i), point);
            if (dist < minDistance) {
                minDistance = dist;
                minIndex = i;
            }
        }
//        System.out.println(minIndex);
        return minIndex;
    }
}
