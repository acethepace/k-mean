import javax.swing.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Akshat on 31-08-2016.
 */
public class Main extends JFrame {

    public static HashMap<Integer, Double> sse = null;
    public static PrintWriter writer = null;

    public static void main(String args[]) {
        System.out.println(DMGMathUtils.sumOfSquaredError(runForK(4, true)));
        System.out.println(DMGMathUtils.sumOfSquaredError(runForK(100, false)));
        sse = new HashMap<>();
        for (int i = 2; i != 13; i++) {
            sse.put(i, DMGMathUtils.sumOfSquaredError(runForK(i, false)));
        }
        try {
            writer = new PrintWriter("ssevsk.csv");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        for (int k : sse.keySet()) {
            String key = String.valueOf(k);
            String value = sse.get(k).toString();
            System.out.println(key + " " + value);
            writer.print(value + ',');
            // series1.add(key,value);

        }
        System.out.println("Writing done forever");
        writer.close();
        // System.out.println(Settings.k);


    }


    public static ArrayList<Cluster> runForK(int k, boolean print) {
        Settings.k = k;
        System.out.println("Reading file...");
        ArrayList<Point> allPoints = readFile();
        Cluster completeCluster = new Cluster(allPoints);
        System.out.println("File read complete, got " + allPoints.size() + " data points");
        ArrayList<Cluster> a = new ArrayList<>();
        a.add(completeCluster);
        double initialSSE = DMGMathUtils.sumOfSquaredError(a);
        ArrayList<Point> means = new ArrayList<>();
        for (int i = 0; i != Settings.k; i++) {
            means.add(allPoints.get(i));
        }
        ArrayList<Cluster> clusters = DMGMathUtils.getClusters(allPoints, means);
//        printClusters(clusters);
        initialSSE = DMGMathUtils.sumOfSquaredError(clusters);
//        System.out.println(initialSSE);
        double newSSE, oldSSE;
        oldSSE = initialSSE;
        newSSE = 0;
        int reduction = 0;
        while (oldSSE - newSSE >= Settings.delta) {
            oldSSE = newSSE;
            means = DMGMathUtils.getMeans(clusters);
            clusters = DMGMathUtils.getClusters(allPoints, means);
            newSSE = DMGMathUtils.sumOfSquaredError(clusters);
            if (print)
                printClusters(clusters);
            reduction++;
        }
        System.out.println("reduced " + reduction + " times for k: " + k);
        return clusters;
    }

    private static void printClusters(ArrayList<Cluster> clusters) {
        clusters.forEach(System.out::println);
    }

    private static ArrayList<Point> readFile() {
        String filepath = "./src/IRIS.csv";
        ArrayList<Point> allPoints = new ArrayList<>();
        String line = "";
        try (BufferedReader br = new BufferedReader(new FileReader(filepath))) {
            int i = 0;
            while ((line = br.readLine()) != null) {
                Point point = new Point(line, i);
                allPoints.add(point);
                i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return allPoints;
    }


}
