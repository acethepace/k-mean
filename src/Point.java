/**
 * Created by Akshat on 02-09-2016.
 */
class Point {
    int pointPosition;

    double[] attributes = new double[Settings.numberOfAttributes];

    Point(int pointPosition) {
        this.pointPosition = pointPosition;
    }

    Point() {
        pointPosition = 0;
    }

    Point(String line, int position) {
        this.pointPosition = position;
        String[] attributesString = line.split(",");
        for (int i = 0; i < Settings.numberOfAttributes; i++) {
            try {
                attributes[i] = Double.parseDouble(attributesString[i]);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public String toString() {
        String string = "(";
        for (double attribute : attributes) {
            string += String.valueOf(attribute);
            string += ",";
        }
        string += ")";
        return string;
    }
}
