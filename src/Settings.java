/**
 * Created by Akshat on 02-09-2016.
 */
class Settings {
    /**
     * k - The number of means to be found.
     */
    public static int k = 4;

    /**
     * numberOfAttributes - The number of attributes of each vector (dimensions)
     */
    static int numberOfAttributes = 8;

    /**
     * delta - the minimum change that must happen to sse for an iteration to be considered
     */
    static double delta = 0.001;
}
